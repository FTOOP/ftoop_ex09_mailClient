package ch.ffhs.ftoop.zh1.mailClient;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 * MailWindow provides functionality to read, forward, reply to, or write new mails.
 * The method initBaseComponents() paints all base components that are equal for both actions read/write.
 * When changing from read to write, the JPanel that is contained in the JFrame is interchanged. This is why the methods invalidate() and validate() are called.
 * 
 * @author Daniel Hoop & Mehrdad Zendehzaban
 * @version 02.06.2017
 */
public class MailWindow extends JFrame {
	
	private int TOTAL_FRAME_WIDTH = 1200;
	private int TOTAL_FRAME_HEIGHT =  600;
	private int LABEL_WIDTH = 60;
	private int TEXT_WIDTH = TOTAL_FRAME_WIDTH - LABEL_WIDTH;
	
	private int LINE_HEIGHT = 30;
	private int NUMBER_OF_LINES = 6;
	private int INSET = 3;
	private int y, x;

	private MailUtils mailer;
	private String userOfAccount;
	private JPanel writePanel = null;
	private JPanel readPanel = null;
	private JTextArea senderTextArea, recipientTextArea, subjectTextArea, mailTextArea;
	private Dimension minTextLineDimension, prefTextLineDimension, prefLabelDimension;

	// Constructor to write mail
	public MailWindow(MailUtils mailer, String userOfAccount){
		super("Write mail");
		prepareBasics();
		this.mailer = mailer;
		this.userOfAccount = userOfAccount;
		SwingUtilities.invokeLater( () -> initWriteWindow(userOfAccount, "", "", "") );
		setVisible(true);
	}

	// Constructor to read mail
	public MailWindow(MailUtils mailer, String userOfAccount, MimeMessage mailMessage, MailAction mailAction){
		super();
		prepareBasics();
		this.mailer = mailer;
		this.userOfAccount = userOfAccount;
		MailTextExtractor mailTexts = new MailTextExtractor( mailMessage );
		
		if(mailAction == MailAction.READ) {
			setTitle("Read mail");
			SwingUtilities.invokeLater( () -> initReadWindow(mailMessage) );
		} else if(mailAction == MailAction.REPLY) {
			setTitle("Reply to mail");
			initWriteWindow( userOfAccount, mailTexts.getFrom(), "Re: "  + mailTexts.getSubject(), mailTexts.writeOldMailContents() );
		} else if(mailAction == MailAction.FORWARD) {
			setTitle("Forward mail");
			initWriteWindow( userOfAccount, "",                  "Fwd: " + mailTexts.getSubject(), mailTexts.writeOldMailContents() );
		}
		
		setVisible(true);
	}

	private void prepareBasics(){
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		setSize(TOTAL_FRAME_WIDTH, TOTAL_FRAME_HEIGHT);
		setLocationRelativeTo(null);
		// Prepare text Area Dimensions
		minTextLineDimension = new java.awt.Dimension(10, LINE_HEIGHT);
		prefTextLineDimension = new java.awt.Dimension(TEXT_WIDTH, LINE_HEIGHT);
		prefLabelDimension = new java.awt.Dimension(LABEL_WIDTH, LINE_HEIGHT);
	}


	private void initReadWindow(MimeMessage mailMessage) {
		writePanel = null;
		
		final MailTextExtractor mailTexts = new MailTextExtractor( mailMessage );

		// Create Panel
		readPanel = initBaseComponents(mailTexts.getFrom(), mailTexts.getRecipients(), mailTexts.getSubject(), mailTexts.getContent(), false);

		// Date
		JTextArea dateTextArea = new JTextArea( mailTexts.getSentDate() );
		dateTextArea.setEditable(false);
		dateTextArea.setMinimumSize(minTextLineDimension);
		java.awt.GridBagConstraints c = new java.awt.GridBagConstraints();
		c.fill = java.awt.GridBagConstraints.BOTH;
		c.anchor = java.awt.GridBagConstraints.WEST;
		c.weightx = 1;
		c.gridx = x;
		c.gridy = ++y;
		readPanel.add(dateTextArea, c);
		
		// Reply JButton
		JButton replyButton = new JButton("Reply");
		replyButton.setMinimumSize(minTextLineDimension);
		replyButton.addActionListener( new ActionListener() { public void actionPerformed(ActionEvent e) {
			SwingUtilities.invokeLater( () -> {
				initWriteWindow( userOfAccount, mailTexts.getFrom(), "Re: " + mailTexts.getSubject(), mailTexts.writeOldMailContents() );
			});
		}});

		c.gridx = ++x;
		c.gridy = y;
		c.anchor = java.awt.GridBagConstraints.CENTER;
		readPanel.add(replyButton, c);
		
		// Forward JButton
		JButton forwardButton = new JButton("Forward");
		forwardButton.setMinimumSize(minTextLineDimension);
		forwardButton.addActionListener( new ActionListener() { public void actionPerformed(ActionEvent e) {
			SwingUtilities.invokeLater( () -> {
				initWriteWindow( userOfAccount, "", "Fwd: " + mailTexts.getSubject(), mailTexts.writeOldMailContents() );
			});
		}});
		c.gridx = ++x;
		c.gridy = y;
		c.anchor = java.awt.GridBagConstraints.EAST;
		readPanel.add(forwardButton, c);

		// Add to pane
		getContentPane().add(readPanel);
		invalidate();
		validate();
		pack();
	}

	private void initWriteWindow(String mailSender, String mailRecipient, String mailSubject, String mailText) {
		// Delete readPanel from conentPane() if it is not null.
		if( readPanel!= null ){
			getContentPane().remove(readPanel);
		}
		writePanel = initBaseComponents(mailSender, mailRecipient, mailSubject, mailText, true);
		// Now set readPanel null. It was necessary to hand over the references for sender, subject, etc.
		readPanel = null;

		// Cancel JButton
		JButton cancelButton = new JButton("Cancel");
		cancelButton.setMinimumSize(minTextLineDimension);
		cancelButton.addActionListener( new ActionListener() { public void actionPerformed(ActionEvent e) {
			SwingUtilities.invokeLater( () -> dispose() );
		}});
		java.awt.GridBagConstraints c = new java.awt.GridBagConstraints();
		c.fill = java.awt.GridBagConstraints.BOTH;
		c.anchor = java.awt.GridBagConstraints.WEST;
		c.weightx = 1;
		c.gridx = x;
		c.gridy = ++y;
		writePanel.add(cancelButton, c);

		// Send JButton
		JButton sendButton = new JButton("Send mail");
		sendButton.setMinimumSize(minTextLineDimension);
		sendButton.addActionListener( new ActionListener() { public void actionPerformed(ActionEvent e) {
			SwingUtilities.invokeLater( () -> {
				setVisible(false);
				Thread thr = new Thread( () -> {
					MimeMessage msg = (MimeMessage) mailer.createMailTryCatch( mailSender, recipientTextArea.getText(), subjectTextArea.getText(), mailTextArea.getText() );
					mailer.sendMailTryCatch(msg);
					try { MailUtils.writeMailToFile(msg, new File(Path.sent() +"/"+ MailUtils.calculateFilenameFromMessage(msg)) );
					} catch (IOException | MessagingException e1) { ExceptionVisualizer.showAndAddMessage(e1, "MailUtils.writeMailToFile()"); }
					//mailer.sendMailTryCatch(mailSender, recipientTextArea.getText(), subjectTextArea.getText(), mailTextArea.getText());
				});
				thr.start();
				try {
					thr.join();
					dispose();
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			});
		}});
		c.anchor = java.awt.GridBagConstraints.EAST;
		c.gridx = ++x;
		c.gridy = y;
		writePanel.add(sendButton, c);

		// Add to pane
		getContentPane().add(writePanel);
		invalidate();
		validate();
		pack();
	}

	private JPanel initBaseComponents(String mailSender, String mailRecipient, String mailSubject, String mailText, boolean setTextAreaEditable) {
		// Create panel
		JPanel wrPanel = new JPanel();
		wrPanel.setLayout(new java.awt.GridBagLayout());

		// Prepare constraints variable for later use.
		java.awt.GridBagConstraints c, cBackup;

		// Prepare x and y
		x = 0;
		y = -1;

		// Sender JLabel
		JLabel senderLabel = new JLabel("Sender");
		senderLabel.setPreferredSize(prefLabelDimension);
		senderLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT); // Schrift festlegen (rechts-, mittig, linksbündig)
		//expressionLabel.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM); // Schrift festlegen (oben, mittig, unten)
		c = new java.awt.GridBagConstraints(); // Reset constraints
		c.gridx = x; // Platz im GridBayLayout x-Achse
		c.gridy = ++y; // Platz im GridBayLayout y-Achse
		c.fill = java.awt.GridBagConstraints.BOTH; // Die Felder sollen vertikal UND horizontal vergrössert werden, wenn das Fenster vergrössert wird!
		c.weightx = 0;   // Welchen Anteil (0-1) soll diese Komponente beim vergrössern auf der x-Achse haben?
		c.weighty = 0; // Welchen Anteil (0-1) soll diese Komponente beim vergrössern auf der y-Achse haben?
		//c.gridwidth = java.awt.GridBagConstraints.RELATIVE; // Am Ende der x-Achse noch eine Zeile frei lassen.
		c.anchor = java.awt.GridBagConstraints.EAST; // Rechts verankern.
		c.insets = new java.awt.Insets(0, INSET, 0, INSET); // Ränder einfügen
		wrPanel.add(senderLabel, c);		

		// Recipient JLabel
		JLabel recipientLabel = new JLabel("Recipient");
		recipientLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT); // Schrift festlegen (rechts-, mittig, linksbündig)
		c.gridy = ++y; // Platz im GridBayLayout y-Achse
		wrPanel.add(recipientLabel, c);

		// Subject JLabel
		// Leave everything the same except gridy!
		JLabel subjectLabel = new JLabel("Subject");
		subjectLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT); // Schrift festlegen (rechts-, mittig, linksbündig)
		c.gridy = ++y; // Platz im GridBayLayout y-Achse
		wrPanel.add(subjectLabel, c);

		cBackup = (GridBagConstraints) c.clone();

		// Text JLabel
		// Here, also the expansion behavior on y axis has to be adjusted!
		JLabel mailTextLabel = new JLabel();
		mailTextLabel.setText("Text");
		mailTextLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT); // Schrift festlegen (rechts-, mittig, linksbündig)
		c.gridy = ++y; // Platz im GridBayLayout y-Achse
		c.weighty = 1;
		wrPanel.add(mailTextLabel, c);

		// Empty JLabel before Buttons
		JLabel actionLabel = new JLabel();
		actionLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT); // Schrift festlegen (rechts-, mittig, linksbündig)
		c = cBackup;
		c.gridy = ++y;
		wrPanel.add(actionLabel, c);


		// Prepare x, y
		x = 1;
		y = -1;

		// Sender JTextArea
		senderTextArea = new JTextArea(mailSender);
		senderTextArea.setEditable(setTextAreaEditable);
		senderTextArea.setMinimumSize(minTextLineDimension);
		senderTextArea.setPreferredSize(prefTextLineDimension);
		// Neues c erstellen, weil es nur horizontal gestreckt werden soll, nicht aber vertikal.
		c = new java.awt.GridBagConstraints();
		c.gridx = x;
		c.gridy = ++y;
		c.weightx = 1; // Ausdehnung auf x Achse festlegen. 
		c.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		c.fill = java.awt.GridBagConstraints.HORIZONTAL;
		c.anchor = java.awt.GridBagConstraints.WEST;
		c.insets = new java.awt.Insets(INSET, 0, 0, INSET); // Ränder einfügen
		wrPanel.add(senderTextArea, c);

		// Recipient JTextArea
		recipientTextArea = new JTextArea(mailRecipient);
		recipientTextArea.setEditable(setTextAreaEditable);
		recipientTextArea.setMinimumSize(minTextLineDimension);
		recipientTextArea.setPreferredSize(prefTextLineDimension);
		c.gridy = ++y;
		wrPanel.add(recipientTextArea, c);

		// Subject JTextArea
		subjectTextArea = new JTextArea(mailSubject);
		subjectTextArea.setEditable(setTextAreaEditable);
		subjectTextArea.setMinimumSize(minTextLineDimension);
		subjectTextArea.setPreferredSize(prefTextLineDimension);
		c.gridy = ++y;
		wrPanel.add(subjectTextArea, c);

		// Mail Test JTextArea
		mailTextArea = new JTextArea(mailText);
		mailTextArea.setEditable(setTextAreaEditable);
		JScrollPane mailTextPane = new JScrollPane(mailTextArea);
		mailTextPane.setMinimumSize(minTextLineDimension);
		mailTextPane.setPreferredSize( new java.awt.Dimension(TEXT_WIDTH, TOTAL_FRAME_HEIGHT-NUMBER_OF_LINES*(INSET+LINE_HEIGHT) )  );
		c = new java.awt.GridBagConstraints();
		c.gridx = x;
		c.gridy = ++y;
		c.fill = java.awt.GridBagConstraints.BOTH; // set it to fill both vertically and horizontally
		c.weightx = 1;
		c.weighty = 1;
		c.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		c.anchor = java.awt.GridBagConstraints.WEST;
		c.insets = new java.awt.Insets(INSET, 0, INSET, INSET); // Ränder auch unten!
		wrPanel.add(mailTextPane, c);

		return wrPanel;
	}
	
}
