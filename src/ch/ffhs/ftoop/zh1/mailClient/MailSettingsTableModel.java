package ch.ffhs.ftoop.zh1.mailClient;


import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;


/**
* MailSettingsTableModel provides the table model to be used in MainSettingsWindow.
* The method updatesettingList() allows to update the list of settings.
* The method addEmtpyRow() provides the functionality to add empty rows such that the number of settings can be increased
* Because empty settings are possible (in empty rows of the settings table) the method addEmtpyRow() allows to return only non-empty settings.
* Vorlage aus Inden S.725
* 
* @author Daniel Hoop & Mehrdad Zendehzaban
* @version 02.06.2017
*/
public class MailSettingsTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;

	// Column names
	private static final String[] COLUMN_NAMES = {"Settings name", "Settings value" };
	private static final Class<?>[] classes = {String.class, String.class};

	// Indices of columns
	private static final int COLUMN_IDX_NAME = 0;
	private static final int COLUMN_IDX_VALUE = 1;

	// Data
	private ArrayList<String[]> settingList;

	// Constructor
	public MailSettingsTableModel(ArrayList<String[]> settingList){
		this.settingList = settingList;
	}
	public MailSettingsTableModel(){
		this.settingList = new ArrayList<>();
		for(int i=0; i<3; i++){
			settingList.add( new String[]{"",""} );
		}
	}
	
	// Method to update settingList
	public void updatesettingList(ArrayList<String[]> settingList){
		this.settingList = settingList;
		fireTableDataChanged(); // Inden S. 741
	}
	// Method to get content that is not empty.
	public ArrayList<String[]> getNonEmptyContent() {
		ArrayList<String[]> outList = new ArrayList<>();
		for(int i=0; i<settingList.size(); i++){
			if( ! (settingList.get(i)[0] + settingList.get(i)[1]).equals("") ) {
				// System.out.println( settingList.get(i)[0] + " : " + settingList.get(i)[1] );
				outList.add( settingList.get(i) );
			}
		}
		return outList;
	}
	// Add empty row to content
	public void addEmtpyRow(){
		settingList.add(new String[]{"",""});
		fireTableDataChanged(); // Inden S. 741
	}
	
	@Override
	public boolean isCellEditable(int row, int column){
		return true;
	}
	@Override
	public String getColumnName(int column){
		return COLUMN_NAMES[column];
	}
	@Override
	public int getColumnCount(){
		return COLUMN_NAMES.length;
	}
	@Override
	public Class<?> getColumnClass(int columnIndex){
		return classes[columnIndex];
	}
	@Override
	public int getRowCount(){
		return settingList.size();
	}
	@Override
	public Object getValueAt(int rowIndex, int columnIndex){
		String[] str = settingList.get(rowIndex);
			if(columnIndex==COLUMN_IDX_NAME)
				return str[0];
			if(columnIndex==COLUMN_IDX_VALUE)
				return str[1];

		throw new IllegalArgumentException("Invalid columnIndex " + columnIndex);
	}
	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex){
		settingList.get(rowIndex)[columnIndex] = (String) value;
	}



}
