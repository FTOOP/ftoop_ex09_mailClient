package ch.ffhs.ftoop.zh1.mailClient;

import java.util.ArrayList;

/**
 * GooglemailUtils was used to develop this application. It stores the right information for a connection to the gmail server.
 * 
 * @author Daniel Hoop & Mehrdad Zendehzaban
 * @version 02.06.2017
 */
@Deprecated
public class ZDELETE_GooglemailUtils extends MailUtils {
	
	// Constructor
	public ZDELETE_GooglemailUtils(String user, String password){
		super(user, password);
	}
	
	public void prepareSessionToReceive(){
		ArrayList<String[]> propertyStringsReceive = new ArrayList<>();
		propertyStringsReceive.add( new String[]{"mail.store.protocol", "pop3s"});
		super.prepareSessionToReceive("pop.gmail.com", propertyStringsReceive);		
	}
	
	public void prepareSessionToSend(){
		ArrayList<String[]> propertyStringsSend = new ArrayList<>();
		propertyStringsSend.add( new String[]{"mail.smtp.auth",            "true"           } );
		propertyStringsSend.add( new String[]{"mail.smtp.starttls.enable", "true"           } );
		propertyStringsSend.add( new String[]{"mail.smtp.host",            "smtp.gmail.com" } );
		propertyStringsSend.add( new String[]{"mail.smtp.port",            "587"            } );
		super.prepareSessionToSend("smtp.gmail.com", propertyStringsSend); 
	}
}
