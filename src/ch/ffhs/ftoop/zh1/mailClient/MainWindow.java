package ch.ffhs.ftoop.zh1.mailClient;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.mail.Message;
import javax.mail.internet.MimeMessage;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

/**
 * MainWindow is the main window for the Mail Client GUI.
 * JTree provides overview of folders. JTable shows all mails. Settings can be changed in the top left corner.
 * Functionalities such as read, reply, forward, move, delete, etc. are provided by JButtons.
 * See detailed description in class Main.
 * 
 * @author Daniel Hoop & Mehrdad Zendehzaban
 * @version 02.06.2017
 *
 */
public class MainWindow extends JFrame {
	
	// TODO Mandatory -> Everything done! See class Main.
	// TODO Optionally -> Some things done. See class Main.
	
	// TODO Optionally, according to exercise
	// TODO Optionally: Make/move/delete new folders.
	
	// TODO Optionally, own
	// TODO Optionally: Load sent messages from online gmail folder
	// TODO Optionally: Table renderer: Bold font for unread mails in table.
	// TODO Optionally: Table renderer: Show part of mail in table row below subject.
	
	// Attributes
	private static final long serialVersionUID = 1L;
	private static final String VERSION = "Version 1.0   Daniel Hoop &  Mehrdad Zendehzaban";
	private MailTableModel mailTableModel;
	private JTable mailTable;

//	private HashMap<String,String> userpw;// = new HashMap<>();
	private MailSettings settingsContainer;
	private MailUtils mailer;
	private int sizeOfActualMailList = 0;
	private String actualFolder = "";
	private JTree tree;
	private int TOTAL_FRAME_WIDTH = 1200;
	private int TOTAL_FRAME_HEIGHT =  600;
	private MainWindow thisWindow = this;

	public MainWindow() {
		super("Mail Client");
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		SwingUtilities.invokeLater( () -> initComponents() );
		SwingUtilities.invokeLater( () -> loginAndLoadMails() );
		
	}
	
	private void loginAndLoadMails(){
		new Thread( () -> {
			// Read mail settings from file if it exists. Then prepare connection.
			if( new File( Path.settings()+"/settings.ser" ).exists() ){
				try { settingsContainer = new FileReadWriteUtils( Path.settings()+"/settings.ser" ).readSerializable( new MailSettings() );
				} catch (Exception e){ ExceptionVisualizer.showAndAddMessage(e, "FileReadWriteUtils(...).readSerializable(...)\n");  }
				connectToMailServer();
			} else {
				settingsContainer = new MailSettings();
				this.mailer = new MailUtils();
			}

			// Load offline mails
			updateTableFromFolderIfNecessary( Path.inbox() );
			// Connect, receive Mails.
			receiveMailsAndUpdateTableIfNecessary();
			
		}).start();
		
		// Update mails from online periodically.
		 ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
		 scheduler.scheduleAtFixedRate( () -> receiveMailsAndUpdateTableIfNecessary(),
				 1, 1, TimeUnit.MINUTES);
		 
	}

	private void initComponents(){

		JPanel contentPane = new JPanel(new BorderLayout());
		//contentPane.setBackground(new Color(113,113,128));
		//contentPane.setBorder(new EmptyBorder(15, 30, 15, 30));
		this.add(contentPane);

		// Erzeugen der Baumstruktur
		tree = new FileTree( Path.mails() );
		// Hinzufuegen von Selectionlistener
		tree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
			public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
				SwingUtilities.invokeLater( () -> {
					TreePath treePath = evt.getPath();
					DefaultMutableTreeNode pathComponent = (DefaultMutableTreeNode) treePath.getLastPathComponent();
					if( pathComponent != null ) {
						updateTableFromFolderIfNecessary( pathComponent.getUserObject().toString().replaceAll("\\\\", "/") );				
					}
				});
			}
		});
		
		// Create menu
		JMenuBar menuBar = new JMenuBar();
		//JMenu menu, submenu;
		//JMenuItem menuItem;
		
		//Build the first menu.
		JMenu menu = new JMenu("Settings");
		try{
			menu.setIcon(scaleIcon( new ImageIcon("images/gearWheel.png") , 10, 10) );
		} catch (Exception e){
			// Alternative: Just don't add the icon.
		}
		menu.setMnemonic(KeyEvent.VK_A);
		menu.getAccessibleContext().setAccessibleDescription( "Settings" );
		//a group of JMenuItems
		JMenuItem menuItem = new JMenuItem(  "POP3 & SMTP", KeyEvent.VK_T  );
		menuItem.setAccelerator(KeyStroke.getKeyStroke(  KeyEvent.VK_1, ActionEvent.ALT_MASK  ));
		menuItem.getAccessibleContext().setAccessibleDescription( "Change POP3 & SMTP settings" );
		menuItem.addActionListener(new ActionListener(){
			@SuppressWarnings("unused")
			public void actionPerformed(ActionEvent evt){
				new MailSettingsWindow( thisWindow, settingsContainer );
			}
		});
		menu.add(menuItem);
		menuBar.add(menu);
		setJMenuBar(menuBar);


		// 2 panel f�r link und mittel Bereich erzeugen 

		JPanel linkerBereich = new JPanel(new FlowLayout( ));
		JPanel mittelbereich = new JPanel(new BorderLayout());

		//border einf�gen .in haupt content addieren+ background farbe

		//linkerBereich.setBorder(new EmptyBorder(90, 15, 5, 15));

		//contentPane.add(linkerBereich  , BorderLayout.WEST);
		JScrollPane jTreePane = new JScrollPane(tree);
		jTreePane.setPreferredSize( new Dimension(180,1) );
		contentPane.add(jTreePane, BorderLayout.WEST);
		contentPane.add(mittelbereich, BorderLayout.CENTER);

		linkerBereich.setBackground(Color.blue);
		mittelbereich.setBackground(Color.white);

		//Komponenten in linke Bereich erst in ein GridLayout dann in ein FlowLayout einf�gen

		JPanel linkerBereichGrid = new JPanel(new GridLayout(0,1));
		//linkerBereichGrid.setBorder(new EmptyBorder(17, 15, 17, 15));
		linkerBereichGrid.setBackground(Color.yellow);
		linkerBereich.add(linkerBereichGrid);

		// Tabelle mit den Mails erzeugen und einfuegen.
		mailTableModel = new MailTableModel(new ArrayList<Message>());
		mailTable = new JTable(mailTableModel);
		//mailTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		mailTableModel.addSorter(mailTable);
	
		// Renderer hinzufuegen
		//mailTable.setDefaultRenderer(String[].class, new MailTableCellRenderer());
		mailTable.setDefaultRenderer(Object.class, new MailTableCellRenderer());
		
		mailTable.getColumnModel().getColumn(0).setPreferredWidth(10);
		// Actionlistener hinzufuegen fuer Doppelclick
		mailTable.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					SwingUtilities.invokeLater( () -> new MailWindow(mailer, settingsContainer.getUser(), (MimeMessage)  getSelectedMessage(), MailAction.READ)  );
				}
			}
		});
		// Dem Mittelbereich zufuegen.
		mittelbereich.add(new JScrollPane(mailTable), BorderLayout.CENTER);







		//Komponenten in mitte Bereich erst in ein FlowLayout dann in ein Bordlayout Nord und rest in 
		// Center un Sout hinzuf�gen

		JPanel menuzeile = new JPanel(new FlowLayout( ));

		JButton tasteNorth = new JButton("Read");
		tasteNorth.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent e) { 
			SwingUtilities.invokeLater( () -> new MailWindow(mailer, settingsContainer.getUser(), (MimeMessage)  getSelectedMessage(), MailAction.READ)  );
		}
		});
		menuzeile.add(tasteNorth);
		menuzeile.add( Box.createRigidArea( new Dimension( 10 , 25 ) )  );



		tasteNorth = new JButton("+ New");
		tasteNorth.addActionListener( new ActionListener() { public void actionPerformed(ActionEvent e) { 
			SwingUtilities.invokeLater( () -> new MailWindow(mailer, settingsContainer.getUser()));
		}});
		menuzeile.add(tasteNorth);
		menuzeile.add( Box.createRigidArea( new Dimension( 10 , 25 ) )  );



		tasteNorth = new JButton("Reply");
		tasteNorth.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent e) { 
			SwingUtilities.invokeLater( () -> new MailWindow(mailer, settingsContainer.getUser(), (MimeMessage)  getSelectedMessage(), MailAction.REPLY)  );
		}
		});
		menuzeile.add(tasteNorth);
		menuzeile.add( Box.createRigidArea( new Dimension( 10 , 25 ) )  );



		tasteNorth = new JButton("Forward");
		tasteNorth.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				SwingUtilities.invokeLater( () -> new MailWindow(mailer, settingsContainer.getUser(), (MimeMessage)  getSelectedMessage(), MailAction.FORWARD)  );
			}
		});
		menuzeile.add(tasteNorth);
		menuzeile.add( Box.createRigidArea( new Dimension( 10 , 25 ) )  );



		tasteNorth = new JButton("Move");
		tasteNorth.addActionListener(new ActionListener() {
			@SuppressWarnings("unused")
			public void actionPerformed(ActionEvent evt) { 
				try { new FileMoverWindow( MailUtils.calculateFilenameFromMessage(getSelectedMessages()), actualFolder, thisWindow );
				} catch (Exception e) { ExceptionVisualizer.show(e); }
			}
		});
		menuzeile.add(tasteNorth);
		menuzeile.add( Box.createRigidArea( new Dimension( 10 , 25 ) )  );

		tasteNorth = new JButton("- Delete");
		tasteNorth.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(Message delMail : getSelectedMessages() ){
					try {
						String fileName = MailUtils.calculateFilenameFromMessage( delMail );
						File fil = new File( actualFolder +"/"+ fileName );
						if(actualFolder.equals( Path.deleted() )){
							fil.delete();
						} else {
							fil.renameTo( new File( Path.deleted() +"/"+ fileName )  );
						}
					} catch ( Exception e1 ) { ExceptionVisualizer.show(e1); }
				}
				
				updateTableFromFolderIfNecessary(actualFolder);
			}
		});
		menuzeile.add(tasteNorth);
		menuzeile.add( Box.createRigidArea( new Dimension( 10 , 25 ) )  );



		tasteNorth = new JButton("Receive mails from server");
		tasteNorth.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater( () -> {
					(new Thread( () -> receiveMailsAndUpdateTableIfNecessary() )). start();
				});
				
			}
		});
		menuzeile.add(tasteNorth);
		menuzeile.add( Box.createRigidArea( new Dimension( 10 , 25 ) )  );
		
		

		// menuzeile in North hinzuf�gen
		mittelbereich.add(menuzeile, BorderLayout.NORTH);

		// ein Label  unten f�r den Statusmeldungen
		JLabel statusLabel = new JLabel(VERSION);
		statusLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		contentPane.add(statusLabel, BorderLayout.SOUTH);

		this.pack();  // This function sets the frame in a size such that all elements are tightly packed.
		this.setSize(TOTAL_FRAME_WIDTH ,TOTAL_FRAME_HEIGHT);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	// Method to update settings for mailing.
	public void updateMailSettings(MailSettings settingsContainer){
		try { new FileReadWriteUtils(Path.settings() + "/settings.ser").writeSerializable(settingsContainer);
		} catch (Exception e){ ExceptionVisualizer.showAndAddMessage(e, "FileReadWriteUtils(...).writeSerializable(...)\n");  }

		this.settingsContainer = settingsContainer;
		connectToMailServer();
	}
	
	// Establish mail session.
	private void connectToMailServer(){
		mailer = new MailUtils( settingsContainer.getUser(), settingsContainer.getPassword(),
				settingsContainer.getReceiveServer(), settingsContainer.getReceiveProperties(),
				settingsContainer.getSendServer(), settingsContainer.getSendProperties() );
	}
	
	// get selected messages
	private Message[] getSelectedMessages(){
		return mailTableModel.getMessageAt(getSelectedRowsFromSortedTable(mailTable));
	}
	private Message getSelectedMessage(){
		return mailTableModel.getMessageAt(getSelectedRowFromSortedTable(mailTable));
	}
	// Get right rows from table selection. This is necessary because of the sorter!
	private int[] getSelectedRowsFromSortedTable(JTable table) {
		int[] selection = table.getSelectedRows();
		for (int i = 0; i < selection.length; i++) {
			selection[i] = table.convertRowIndexToModel(selection[i]);
		}
		return selection;
	}
	private int getSelectedRowFromSortedTable(JTable table) {
		return table.convertRowIndexToModel( table.getSelectedRow() );
	}
	   
	
	// Method to update mailList
	private void receiveMailsAndUpdateTableIfNecessary(){
		// Get all new mails from online, store them offline and read all mails from offline folder. Draw new table if mails have changed.
		mailer.receiveMailsTryCatch("inbox", Path.inbox(), true);
		updateTableFromFolderIfNecessary( Path.inbox() );
	}

	// package visibility is necessary for FileMoverWindow.
	void updateTableFromFolderIfNecessary(String folder){
		ArrayList<Message> mailList = new ArrayList<Message>( Arrays.asList( mailer.readMailsFromFolderTryCatch(folder) ) );
		if( actualFolder != folder || sizeOfActualMailList != mailList.size() ){
			mailTableModel.updateMailList(	mailList );
			sizeOfActualMailList = mailList.size();
			actualFolder = folder;
			if( mailTable.getModel().getRowCount()>0 ){
				mailTable.changeSelection(0,0,false, false);
			}
		}
	}

	private ImageIcon scaleIcon(ImageIcon imageIcon, int width, int height){
		Image image = imageIcon.getImage(); // transform it 
		Image newimg = image.getScaledInstance(width, height,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		return new ImageIcon(newimg);  // transform it back		
	}

}
