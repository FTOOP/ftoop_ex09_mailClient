package ch.ffhs.ftoop.zh1.mailClient;

/**
 *  Die  folgende Programm implementiert ein Email-Programm Anwendung.
 *  Gem�� Aufgabe 9 von der Aufgabensammlung FTOOP, das Hauptfenster  besteht aus zwei Bereichen.
 *  Einer Ordner�bersicht mit hierarchischen Ordnern, in denen Mails abgelegt ist,
 *  Sowie einem zweiten Bereich mit der �bersichtsliste der Mails.
 *  Aus der �bersichtsliste kann einzelnes Mail angeklickt werden, um ein Fenster mit der
 *  vollst�ndigen Ansicht zu �ffnen. Dort kann die Mail gelesen, beantwortet oder weitergeleitet werden.
 *  Zum Versenden von Mails wird  ein separates Fenster ge�ffnet.
 *  Ein weiteres Fenster dient dazu, die Verbindungsparameter zum Mailserver einzustellen.
 *  Dieses Fenster is �ber das Men�band "Settings" erreichbar.
 *  .........................................................................................................
 *  Die  Anwendung verf�gt folgende Funktionen:
 *  - Abholen von Mails von einem POP3-Server
 *  - Einsortieren von abgeholten Mails in einen Standard-Ordner �Neue Mails�
 *  - �ffnen einer Detailansicht von Mails in einem neuen Fenster mit Antwort/Weiterleitungsfunktion
 *  - Erstellen von neuen Mails und Senden via SMTP
 *  - Beantworten und weiterleiten von Mails
 *  - Konfiguration von POP3- sowie SMTP-Zugangsdaten
 *  
 *  - Automatisches Abholen per Zeitintervall (1 Minute)
 *  - L�schen von Mails erst Verschieben in Ordner "deleted". Wenn dort nochmal gel�scht, dann endg�ltig.
 *  - Verschieben von Mails in einen existierenden Ordner
 *
 * @author Daniel Hoop & Mehrdad Zendehzaban
 * @version 02.06.2017
 */
public class Main {
	@SuppressWarnings("unused")
	public static void main(String[] args) throws Exception {
		new Main();
	}

	@SuppressWarnings("unused")
	public Main() throws Exception {
		// Set look and feel.
		LookAndFeelUtils.set();
		// Create and display the form
		java.awt.EventQueue.invokeLater(new Runnable() {  public void run() { new MainWindow(); } });
	}
}

