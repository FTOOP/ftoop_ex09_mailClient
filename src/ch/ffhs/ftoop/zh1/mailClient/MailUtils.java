package ch.ffhs.ftoop.zh1.mailClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.NoSuchFileException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
* MailUtils connects to the mail server, sends and receives mails.
* Detail: MailUtils is not really a Utility class because the methods are not static.
* 
* @author Daniel Hoop & Mehrdad Zendehzaban
* @version 02.06.2017
*/
public class MailUtils {
	// TODO: Siehe Inden, S. 1005, Kapitel 16.3.11 -> prepareSessiontoReceive, etc. in Konstruktor

	// Declaration of attributes.
	protected String receiveMailServer, sendMailServer;
	protected String user, password;
	protected Session receiveSession, sendSession;


	/**
	 * Minimal constructor in case only connection to pop server is needed. This can be specified by prepareSessionToReceive() or prepareSessionToSend().
	 * @param user
	 * @param password
	 */
	public MailUtils(String user, String password){
		this.user = user;
		this.password = password;
	}
	
	/**
	 * Normal constructor that contains all information that is necessary to receive and send mails.
	 * @param user
	 * @param password
	 * @param receiveMailServer
	 * @param receiveProperties
	 * @param sendMailServer
	 * @param sendProperties
	 */
	public MailUtils(String user, String password, String receiveMailServer, ArrayList<String[]> receiveProperties, String sendMailServer, ArrayList<String[]> sendProperties){
		this(user, password);
		prepareSessionToReceive(receiveMailServer, receiveProperties);
		prepareSessionToSend(sendMailServer, sendProperties);
	}
	
	/**
	 * Empty constructor if settings are not available but the object is needed to run an application.
	 */
	public MailUtils(){
		this.user = "";
		this.password = "";
		this.receiveMailServer = "";
		this.sendMailServer = "";
		this.receiveSession = Session.getDefaultInstance( System.getProperties() );
		this.sendSession = Session.getDefaultInstance( System.getProperties() );
	}

	/**
	 * The methods prepareSessionToReceive and prepareSessionToSend are provided publicly because classes that might inherit from MailUtils
	 * might not be able to prepare all information before calling super() in the constructor. This is the case in GooglemailUtils.
	 * @param mailServer
	 * @param receiveProperties
	 */
	public void prepareSessionToReceive(String mailServer, ArrayList<String[]> receiveProperties){
		this.receiveMailServer = mailServer;
		receiveSession = Session.getDefaultInstance( getSystemPropertiesAndAddSpecificEntries(receiveProperties) ) ;		
	}
	/**
	 * The methods prepareSessionToReceive and prepareSessionToSend are provided publicly because classes that might inherit from MailUtils
	 * might not be able to prepare all information before calling super() in the constructor. This is the case in GooglemailUtils.
	 * @param mailServer
	 * @param receiveProperties
	 */
	public void prepareSessionToSend(String mailServer, ArrayList<String[]> sendProperties){
		this.sendMailServer = mailServer;
		sendSession = Session.getDefaultInstance( getSystemPropertiesAndAddSpecificEntries(sendProperties) ) ;
	}
	
	/**
	 * Method to create a text mail.
	 * @param from
	 * @param to
	 * @param subject
	 * @param text
	 * @return
	 * @throws MessagingException
	 */
	public Message createMail(String from, String to, String subject, String text) throws MessagingException {
		// Create a default Message object
		Message message = new MimeMessage( sendSession ) ;

		// Set some attributes
		message.setFrom( new InternetAddress(from) ) ;
		message.addRecipient( Message.RecipientType.TO, new InternetAddress(to) ) ;
		message.setSubject( subject ) ;
		message.setText( text ) ;
		
		// Return message.
		return message;
	}
	public Message createMailTryCatch(String from, String to, String subject, String text) {
		try { return createMail(from, to, subject, text);
		} catch( MessagingException e ) { ExceptionVisualizer.showAndAddMessage(e, "MailUtils.createMail(...);\n"); }
		
		return null;
	}

	/**
	 * Method to send a message
	 * @param message
	 * @throws MessagingException
	 */
	public void sendMail(Message message) throws MessagingException {
		// Send the mail
		Transport transport = sendSession.getTransport( "smtps" ) ;
		try {
			transport.connect( sendMailServer, user, password ) ;
			transport.sendMessage( message, message.getAllRecipients() ) ;
		} finally {
			transport.close() ;
		}
	}
	/**
	 * Method sends text only mails.
	 * @param from
	 * @param to
	 * @param subject
	 * @param text
	 * @throws MessagingException
	 */
	public void sendMail(String from, String to, String subject, String text) throws MessagingException {
		// Create the mail & send it.
		sendMail(createMail(from, to, subject, text));
		
	}
	public void sendMailTryCatch(String from, String to, String subject, String text) {
		try { sendMail(from, to, subject, text);
		} catch( MessagingException e ) { ExceptionVisualizer.showAndAddMessage(e, "MailUtils.sendMail(...);\n"); }
	}
	public void sendMailTryCatch(Message message) {
		try { sendMail(message);
		} catch( MessagingException e ) { ExceptionVisualizer.showAndAddMessage(e, "MailUtils.sendMail(...);\n"); }
	}

	public void receiveMailsTryCatch(String fromFolder, String toFolder, boolean printMailDetails){
		try{
			receiveMails(fromFolder, toFolder, printMailDetails);
		} catch (Exception e){ ExceptionVisualizer.showAndAddMessage(e, "MailUtils.receiveMails(...);\n"); }
	}
	
	/**
	 * The method receives mails from a specified online mail folder. The mails are stored in a "real" folder in the file system, but only if their not already stored.
	 * The method calculateFilenameFromMessage() is called which gives each mail a unique filename considerung "from", "to", "subject" and "date sent".
	 * The mails are not deleted from the online folder!
	 *   
	 * @param fromFolder
	 * @param toFolder
	 * @param printMailDetails
	 * @throws MessagingException
	 * @throws NoSuchFileException
	 * @throws IOException
	 */
	public void receiveMails(String fromFolder, String toFolder, boolean printMailDetails)  throws MessagingException, NoSuchFileException, IOException {
		Message[] messages;
		Message msg;
		String fileName;
		File fileMail;

		// Get a store for the POP3S protocol
		Store store = receiveSession.getStore() ;
		// Connect to the current receiveMailServer using the specified username and password
		store.connect( receiveMailServer, user, password ) ;

		// Create a Folder object corresponding to the given name
		Folder folder = store.getFolder( fromFolder ) ;
		// Open the Folder
		folder.open( Folder.READ_WRITE ) ;

		// Get the messages from the server
		messages = folder.getMessages();
		// Save messages to the computer file system.
		for( int i = 0; i < messages.length; i++ ) {
			msg = messages[i] ;
			fileName = toFolder +"/"+ calculateFilenameFromMessage(msg, printMailDetails);
			fileMail = new File(fileName);
			// Only save if not already exists on hard disk
			if(!fileMail.exists()){
				writeMailToFile(msg, fileMail);
			}
			/* In der endgueltigen Version sollen die Mails geloescht werden */
			// msg.setFlag( Flags.Flag.DELETED, true ) ;
		}
		folder.close( true );
		store.close();
	}
	
	public static void writeMailToFile(Message msg, File file) throws IOException, MessagingException {
		FileOutputStream outputStreamMail = new FileOutputStream(file);
		try{
			msg.writeTo( outputStreamMail );
		} finally {
			outputStreamMail.flush();
			outputStreamMail.close();
		}
	}

	public Message[] readMailsFromFolderTryCatch(String folderName) {
		Message[] messages;
		try{
			messages = readMailsFromFolder(folderName);
		} catch (Exception e) {
			ExceptionVisualizer.showAndAddMessage(e, "MailUtils.readMailsFromFolder(...);\n");
			messages = new Message[1];
		}
		return messages;
	}

	public Message[] readMailsFromFolder(String folderName) throws FileNotFoundException {
		
		Session session = Session.getDefaultInstance(System.getProperties(), null);
		File folderFile = new File(folderName);
		if(!folderFile.exists()) {
			throw new FileNotFoundException();
		}	

		File[] files = folderFile.listFiles();
		Message[] messages = new Message[files.length];
		int indexOfNewMail = -1;
		try {
			InputStream mailFileInputStream;
			
			for(int i=0; i<files.length; i++){
				if(!files[i].isDirectory() && files[i].getName().toLowerCase().matches(".*eml")){
					indexOfNewMail++;
					mailFileInputStream = new FileInputStream( files[i] );
					messages[indexOfNewMail] = new MimeMessage(session, mailFileInputStream);
					mailFileInputStream.close();
				}
			}
			messages = Arrays.copyOfRange(messages, 0, indexOfNewMail+1);
		} catch (Exception e) { ExceptionVisualizer.showAndAddMessage(e, "MailUtils.readMailsFromFolder(...);\n"); }

		return messages;
	}

//	private Properties getSystemPropertiesAndAddSpecificEntries( HashMap<String,String> specificEntries ) {
//		// Get system Properties
//		Properties properties = System.getProperties() ;
//		// Put all entries of the HashMap<String,String> propertyStrings into receiveProperties. 
//		Iterator<Entry<String, String>> it = specificEntries.entrySet().iterator();
//		while (it.hasNext()) {
//			Map.Entry<String, String> pair = (Map.Entry<String, String>) it.next();
//			properties.put(pair.getKey(), pair.getValue()); 
//			it.remove(); // avoids a ConcurrentModificationException
//		}
//		return properties;
//	}
	
	private Properties getSystemPropertiesAndAddSpecificEntries( ArrayList<String[]> specificEntries ) {
		Properties properties = System.getProperties();
		for(int i=0; i<specificEntries.size(); i++){
			properties.put( specificEntries.get(i)[0], specificEntries.get(i)[1] );
		}
		return properties;
	}
	public static String[] calculateFilenameFromMessage(Message[] msg) throws MessagingException, IOException {
		String[] fileNames = new String[msg.length];
		for(int i=0; i<msg.length; i++){
			fileNames[i] = calculateFilenameFromMessage(msg[i]);
		}
		return fileNames;
	}
	public static String calculateFilenameFromMessage(Message msg) throws MessagingException, IOException {
		return calculateFilenameFromMessage(msg, false);
	}
	
	public static String calculateFilenameFromMessage(Message msg, boolean printMailDetails) throws MessagingException, IOException {
		StringBuilder fileName = new StringBuilder();
		int NCHARS = 12;
		
		Date sent = msg.getSentDate() ;
		if( sent == null ){
			msg.setSentDate( new Date() );
			sent = msg.getSentDate() ;
		}
		if(printMailDetails) System.out.println( "Sent: " + sent ) ;
		fileName.append(repaceForbiddenChars( new SimpleDateFormat("yyyyMMdd-HHmmss").format(sent),  200));
			
		String from = InternetAddress.toString( msg.getFrom() ) ;
		if( from != null ){
			if(printMailDetails) System.out.println( "From: " + from ) ;
			fileName.append("_" + repaceForbiddenChars(from,NCHARS));
		}
		String to = InternetAddress.toString( msg.getRecipients(Message.RecipientType.TO) ) ;
		if( to != null ){
			if(printMailDetails) System.out.println( "To: " + to ) ;
			fileName.append("_" + repaceForbiddenChars(to,NCHARS));
		}
		String subject = msg.getSubject() ;
		if( subject != null ){
			if(printMailDetails) System.out.println( "Subject: " + subject ) ;
			fileName.append("_" + repaceForbiddenChars(subject,NCHARS));
		}
		fileName.append(".eml");

		if(printMailDetails) System.out.println() ;
		if(printMailDetails) System.out.println( msg.getContent() ) ;

		return fileName.toString();
	}

	private static String repaceForbiddenChars(String s, int maxStringLength){
		return s.replaceAll("[^a-zA-Z0-9@]", "_").substring(0, Math.min(s.length(),maxStringLength));
	}
} 
