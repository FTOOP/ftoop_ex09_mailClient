package ch.ffhs.ftoop.zh1.mailClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.NoSuchFileException;

import java.security.NoSuchProviderException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * A simple program for receiving mail messages
 * from gmail using POP3S.
 *
 * @version 02-APR-2015
 */
@Deprecated
public class ZDELETE_MailUtilsWithHashMap {
	// TODO: Siehe Inden, S. 1005, Kapitel 16.3.11 -> prepareSessiontoReceive, etc. in Konstruktor

	// Declaration of attributes.
	protected String receiveMailServer, sendMailServer;
	protected String user, password;
	protected Session receiveSession, sendSession;


	public ZDELETE_MailUtilsWithHashMap(String user, String password){
		this.user = user;
		this.password = password;
	}

	public void prepareSessionToReceive(String mailServer, HashMap<String,String> receiveProperties){
		this.receiveMailServer = mailServer;
		receiveSession = Session.getDefaultInstance( getSystemPropertiesAndAddSpecificEntries(receiveProperties) ) ;		
	}

	public void prepareSessionToSend(String mailServer, HashMap<String,String> sendProperties){
		this.sendMailServer = mailServer;
		sendSession = Session.getDefaultInstance( getSystemPropertiesAndAddSpecificEntries(sendProperties) ) ;
	}

	public void sendMail(String from, String to, String subject, String text) throws MessagingException {
		//		try {
		// Create a default MimeMessage object
		MimeMessage message = new MimeMessage( sendSession ) ;

		// Set some attributes
		message.setFrom( new InternetAddress(from) ) ;
		message.addRecipient( MimeMessage.RecipientType.TO, new InternetAddress(to) ) ;
		message.setSubject( subject ) ;
		message.setText( text ) ;

		// Send the mail
		Transport transport = sendSession.getTransport( "smtps" ) ;
		try {
			transport.connect( sendMailServer, user, password ) ;
			transport.sendMessage( message, message.getAllRecipients() ) ;
		} finally {
			transport.close() ;
		}
	}

	public void sendMailTryCatch(String from, String to, String subject, String text) {
		try {
			sendMail(from, to, subject, text);
		} catch( MessagingException e ) { ExceptionVisualizer.showAndAddMessage(e, "MailUtils.sendMail(...);\n"); }
	}

	public void receiveMailsTryCatch(String fromFolder, String toFolder, boolean printMailDetails){
		try{
			receiveMails(fromFolder, toFolder, printMailDetails);
		} catch (Exception e){ ExceptionVisualizer.showAndAddMessage(e, "MailUtils.receiveMails(...);\n"); }
	}

	public void receiveMails(String fromFolder, String toFolder, boolean printMailDetails)  throws MessagingException, NoSuchProviderException, NoSuchFileException, IOException {
		Message[] messages;
		MimeMessage msg;
		String fileName;
		File fileMail;
		FileOutputStream outputStreamMail;

		// Get a store for the POP3S protocol
		Store store = receiveSession.getStore() ;
		// Connect to the current receiveMailServer using the specified username and password
		store.connect( receiveMailServer, user, password ) ;

		// Create a Folder object corresponding to the given name
		Folder folder = store.getFolder( fromFolder ) ;
		// Open the Folder
		folder.open( Folder.READ_WRITE ) ;

		// Get the messages from the server
		messages = folder.getMessages();
		// Save messages to the computer file system.
		for( int i = 0; i < messages.length; i++ ) {
			msg = (MimeMessage) messages[i] ;
			fileName = toFolder +"/"+ calculateFilenameFromMimeMessage(msg, printMailDetails);
			fileMail = new File(fileName);
			// Only save if not already exists on hard disk
			if(!fileMail.exists()){
				outputStreamMail = new FileOutputStream(fileMail);
				try{
					msg.writeTo( outputStreamMail );
				} finally {
					outputStreamMail.flush();
					outputStreamMail.close();
				}
			}
			/* In der endgueltigen Version sollen die Mails geloescht werden */
			// msg.setFlag( Flags.Flag.DELETED, true ) ;
		}
		folder.close( true );
		store.close();

	}

	public MimeMessage[] readMailsFromFolderTryCatch(String folderName) {
		MimeMessage[] messages;
		try{
			messages = readMailsFromFolder(folderName);
		} catch (Exception e) {
			ExceptionVisualizer.showAndAddMessage(e, "MailUtils.readMailsFromFolder(...);\n");
			messages = new MimeMessage[1];
		}
		return messages;
	}

	public MimeMessage[] readMailsFromFolder(String folderName) throws FileNotFoundException {
		
		Session session = Session.getDefaultInstance(System.getProperties(), null);
		File folderFile = new File(folderName);
		if(!folderFile.exists()) {
			throw new FileNotFoundException();
		}	

		File[] files = folderFile.listFiles();
		MimeMessage[] messages = new MimeMessage[files.length];
		int indexOfNewMail = -1;
		try {
			InputStream mailFileInputStream;
			
			for(int i=0; i<files.length; i++){
				if(!files[i].isDirectory() && files[i].getName().toLowerCase().matches(".*eml")){
					indexOfNewMail++;
					mailFileInputStream = new FileInputStream( files[i] );
					messages[indexOfNewMail] = new MimeMessage(session, mailFileInputStream);
					mailFileInputStream.close();
				}
			}
			messages = Arrays.copyOfRange(messages, 0, indexOfNewMail+1);
		} catch (Exception e) { ExceptionVisualizer.showAndAddMessage(e, "MailUtils.readMailsFromFolder(...);\n"); }

		return messages;
	}

	private Properties getSystemPropertiesAndAddSpecificEntries( HashMap<String,String> specificEntries ) {
		// Get system sendProperties
		Properties properties = System.getProperties() ;
		// Put all entries of the HashMap<String,String> propertyStrings into receiveProperties. 
		Iterator<Entry<String, String>> it = specificEntries.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String,String> pair = (Map.Entry<String,String>) it.next();
			properties.put(pair.getKey(), pair.getValue()); 
			it.remove(); // avoids a ConcurrentModificationException
		}
		return properties;
	}

	public String calculateFilenameFromMimeMessage(MimeMessage msg, boolean printMailDetails) throws MessagingException, IOException {
		StringBuilder fileName = new StringBuilder();

		Date sent = msg.getSentDate() ;
		if( sent != null ){
			if(printMailDetails) System.out.println( "Sent: " + sent ) ;
			fileName.append(repaceForbiddenChars(sent.toString().substring(4),200)); // Ignore week day (first few characters)
		}
		String from = InternetAddress.toString( msg.getFrom() ) ;
		if( from != null ){
			if(printMailDetails) System.out.println( "From: " + from ) ;
			fileName.append("-" + repaceForbiddenChars(from,8));
		}
		String to = InternetAddress.toString( msg.getRecipients(MimeMessage.RecipientType.TO) ) ;
		if( to != null ){
			if(printMailDetails) System.out.println( "To: " + to ) ;
			fileName.append("-" + repaceForbiddenChars(to,8));
		}
		String subject = msg.getSubject() ;
		if( subject != null ){
			if(printMailDetails) System.out.println( "Subject: " + subject ) ;
			fileName.append("-" + repaceForbiddenChars(subject,8));
		}
		fileName.append(".eml");

		if(printMailDetails) System.out.println() ;
		if(printMailDetails) System.out.println( msg.getContent() ) ;

		return fileName.toString();
	}

	private String repaceForbiddenChars(String s, int maxStringLength){
		return s.replaceAll("[^a-zA-Z0-9.-]", "_").substring(0, Math.min(s.length(),maxStringLength));
	}
} 
