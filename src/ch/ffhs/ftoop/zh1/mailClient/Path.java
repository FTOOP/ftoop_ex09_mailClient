package ch.ffhs.ftoop.zh1.mailClient;

/**
 * This class stores all paths (directories) that are used for mails, etc.
 * @author Daniel Hoop
 *
 */
public class Path {
	
	// 1
	public static String dataGeneral(){
		return "data";
	}
	// 1.1
	public static String settings(){
		return dataGeneral() + "/settings";
	}
	// 1.2
	public static String mails(){
		return dataGeneral() + "/mails";
	}
	// 1.2.1
	public static String inbox(){
		return mails() + "/inbox";
	}
	// 1.2.2
	public static String sent(){
		return mails() + "/sent";
	}
	// 1.2.3
	public static String deleted(){
		return mails() + "/deleted";
	}
	
	
}
