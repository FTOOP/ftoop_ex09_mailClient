package ch.ffhs.ftoop.zh1.mailClient;

import java.io.File;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 * FileTree represents file/directory structures in a JTree.
 * The tree is built by recursion.
 *
 * @author Daniel Hoop & Mehrdad Zendehzaban
 * @version 02.06.2017
 */
public class FileTree extends JTree {
	private static final long serialVersionUID = 1L;

	public FileTree(String rootNode){
		super();
		updateTreeStructure(rootNode);
	}
	
	
	public void updateTreeStructure(String rootNode) {
		DefaultMutableTreeNode root = getRecursiveDefaultMutableTreeNode(new File(rootNode));
		DefaultTreeModel model = new DefaultTreeModel(root);
		setModel(model); // inherited from JTree
	}
	
	private DefaultMutableTreeNode getRecursiveDefaultMutableTreeNode(File fileRoot){
		
		DefaultMutableTreeNode root = new DefaultMutableTreeNode(fileRoot);

		File[] subItems = fileRoot.listFiles();
		for (File file : subItems) {
			if(file.isDirectory()){
				root.add(getRecursiveDefaultMutableTreeNode(file));				
			}
		}
		return root;
	}
	
}
