package ch.ffhs.ftoop.zh1.mailClient;


import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
/**
 * ExceptionVisualizer takes different kinds of exceptions and shows them in a JOptionPane as information.
 * Optionally, own messages can be added to make debugging easier.
 * 
 * @author Daniel Hoop & Mehrdad Zendehzaban
 * @version 02.06.2017
 */
public class ExceptionVisualizer {
	public static void showAndAddMessage(Exception e, String message){
		SwingUtilities.invokeLater( () -> JOptionPane.showMessageDialog(null, message + e.getMessage(), getExceptionType(e), JOptionPane.INFORMATION_MESSAGE) );
		e.printStackTrace();
	}
	public static void showWithOwnMessage(Exception e, String message){
		SwingUtilities.invokeLater( () -> JOptionPane.showMessageDialog(null, message, getExceptionType(e), JOptionPane.INFORMATION_MESSAGE) );
		e.printStackTrace();
	}
	public static void show(Exception e){
		SwingUtilities.invokeLater( () -> JOptionPane.showMessageDialog(null, e.getMessage(), getExceptionType(e), JOptionPane.INFORMATION_MESSAGE) );
		e.printStackTrace();
	}
	private static String getExceptionType(Exception e){
		if( e instanceof java.io.FileNotFoundException ) {
			return "java.io.FileNotFoundException";
		} else if( e instanceof java.nio.file.NoSuchFileException ) {
			return "java.nio.file.NoSuchFileException";
		} else if( e instanceof java.io.IOException ) {
			return "java.io.IOException";
		} else if( e instanceof java.security.NoSuchProviderException ) {
			return "java.security.NoSuchProviderException";
		} else if( e instanceof javax.mail.MessagingException ) {
			return "javax.mail.MessagingException";
		} else if ( e instanceof java.lang.ClassNotFoundException ) {
			return "java.lang.ClassNotFoundException"; 
		} else {
			return "Exception";
		}
		
	}
}
