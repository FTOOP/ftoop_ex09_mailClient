package ch.ffhs.ftoop.zh1.mailClient;
import java.io.Serializable;
import java.util.ArrayList;

/**
* MailSettings stores the data that is needed to connect to a mail server.
*
* @author Daniel Hoop & Mehrdad Zendehzaban
* @version 02.06.2017
*/
public class MailSettings implements Serializable {
	
	private String user, password, receiveServer, sendServer;
	private ArrayList<String[]> receiveProperties, sendProperties;
	
	// Normal constructor.
	public MailSettings(String user, String password,
			String receiveServer, String sendServer,
			ArrayList<String[]> receiveProperties, ArrayList<String[]> sendProperties){
		this.user = user;
		this.password = password;
		this.receiveServer = receiveServer;
		this.sendServer = sendServer;
		this.receiveProperties = receiveProperties;
		this.sendProperties = sendProperties;
	}
	// Empty constructor.
	public MailSettings(){
		this.user = "";
		this.password = "";
		this.receiveServer = "";
		this.sendServer = "";
		this.receiveProperties = new ArrayList<>();
		this.sendProperties = new ArrayList<>();
	}
	
	public String getUser(){
		return user;
	}
	public String getPassword(){
		return password;
	}
	public String getReceiveServer(){
		return receiveServer;
	}
	public String getSendServer(){
		return sendServer;
	}
	public ArrayList<String[]> getReceiveProperties(){
		return receiveProperties;
	}
	public ArrayList<String[]> getSendProperties(){
		return sendProperties;
	}	
	
}
