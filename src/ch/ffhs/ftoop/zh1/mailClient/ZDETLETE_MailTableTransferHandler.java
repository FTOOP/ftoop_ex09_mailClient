package ch.ffhs.ftoop.zh1.mailClient;

import java.awt.datatransfer.Transferable;
import java.io.File;

import javax.mail.Message;
import javax.mail.internet.MimeMessage;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.TransferHandler;

@Deprecated
public class ZDETLETE_MailTableTransferHandler extends TransferHandler {
    private FileTree tree;
    private int addIndex = -1; //Location where items were added
    private int addCount = 0;  //Number of items added.
    
    public ZDETLETE_MailTableTransferHandler(FileTree tree) {
    	this.tree = tree;
    }
    
    @Override
    public boolean canImport(TransferHandler.TransferSupport info) {
    	return false;
   }

//    @Override
//    protected Transferable createTransferable(JComponent comp) {
//    	JTable mailTable = (JTable) comp;
//    	MailTableModel mailTableModel = (MailTableModel) mailTable.getModel();
//        return (Transferable) mailTableModel.getMessageAt((mailTable.getSelectedRows()));
//     }
    
    @Override
    public int getSourceActions(JComponent c) {
        return TransferHandler.COPY_OR_MOVE;
    }
    
    @Override
    public boolean importData(TransferHandler.TransferSupport info) {
    	return false;
    }

    /**
     * Remove the items moved from the list.
     */
    protected void exportDone(JComponent comp, Transferable data, int action) {
    	JTable mailTable = (JTable) comp;
    	MailTableModel mailTableModel = (MailTableModel) mailTable.getModel();

        if (action == TransferHandler.MOVE) {
        	
        	for(Message delMail : mailTableModel.getMessageAt((mailTable.getSelectedRows())) ){
				try {
					String fileName = MailUtils.calculateFilenameFromMessage( (MimeMessage) delMail );
					File fil = new File( "actualFolder" +"/"+ fileName );
					if("actualFolder".equals("data/mails/deleted")){
						fil.delete();
					} else {
						fil.renameTo( new File( "data/mails/deleted" +"/"+ fileName )  );
					}
				} catch ( Exception e1 ) { ExceptionVisualizer.show(e1); }
			}
        	
        }
        
        addCount = 0;
        addIndex = -1;
    }
}