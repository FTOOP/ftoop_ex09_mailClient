package ch.ffhs.ftoop.zh1.mailClient;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
* MailSettingsWindow provides a GUI that allows to change the mail settings (to connect to the mail server).
* It depends on the classes MailSettingsTableModel and MailSettings.
* Note:
* Before the settings are stored, it is important to call  JTable.getCellEditor().stopCellEditing()!
* Otherwise the last table cell that was edited might not store the newly entered data. 
* 
* @author Daniel Hoop & Mehrdad Zendehzaban
* @version 02.06.2017
*/
public class MailSettingsWindow extends JFrame {

	private int TOTAL_FRAME_WIDTH = 500;
//	private int TOTAL_FRAME_HEIGHT =  600;
//	private int LINE_HEIGHT = 30;
//	private int NUMBER_OF_LINES = 5;
//	private int INSET = 3;
	private MainWindow mainWindow;
	private MailSettings settingsContainer;
	private MailSettingsWindow thisWindow;
	
	@SuppressWarnings("unused")
	public static void main(String[] args){
		new MailSettingsWindow();
	}
	
	private MailSettingsWindow(){
		super("Settings");
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		
		this.thisWindow = this;
		this.settingsContainer = new MailSettings();
		SwingUtilities.invokeLater( () -> initComponents() );
	}
	
	public MailSettingsWindow( MainWindow mainWindow, MailSettings settingsContainer ){
		this();
		this.mainWindow = mainWindow;
		this.settingsContainer = settingsContainer;
	}
	
	
	
	private void initComponents(){
		GridBagConstraints c;
		int x = 0;
		int y = -1;
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		
		JLabel title = new JLabel("Please edit your mail settings below.");
		Font f = title.getFont(); 
		title.setFont(new Font( f.getFontName(), Font.BOLD, f.getSize()+4 ));
		
		//JViewport viewport = infoTextScrollPane.getViewport();
		//viewport.setScrollMode(JViewport.BLIT_SCROLL_MODE); viewport.setScrollMode(JViewport.BACKINGSTORE_SCROLL_MODE); viewport.setScrollMode(JViewport.SIMPLE_SCROLL_MODE);
		
		c = new GridBagConstraints();
		c.fill = java.awt.GridBagConstraints.BOTH;
		c.anchor = java.awt.GridBagConstraints.WEST;
		c.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		c.weightx = 1; c.weighty = 0;
		c.gridx = x;
		c.gridy = ++y;
		c.anchor = java.awt.GridBagConstraints.WEST; // Links verankern.
		
		panel.add(title, c);
		
		// User and password. Label & TextField
		JLabel labelUser = new JLabel("User:");
		JLabel labelPassword = new JLabel("Password:");
		JLabel labelPopServer = new JLabel("Server to receive mails:");
		JLabel labelSmtServer = new JLabel("Server to send mails:");
		JTextField txtUser = new JTextField( settingsContainer.getUser() );
		JTextField txtPassword = new JTextField( settingsContainer.getPassword() );
		JTextField txtPopServer =  new JTextField( settingsContainer.getReceiveServer() );
		JTextField txtSmtServer =  new JTextField( settingsContainer.getSendServer() );
		
		
		// First set labels
		c.gridwidth = 1;
		c.weightx = 0.2; c.weighty = 0;
		c.gridx = x;
		c.gridy = ++y; panel.add(new JLabel(" "),c);
		int yBackup = y;
		c.gridy = ++y; panel.add(labelUser,c);
		c.gridy = ++y; panel.add(labelPassword,c);
		c.gridy = ++y; panel.add(labelPopServer, c);
		c.gridy = ++y; panel.add(labelSmtServer, c);
		
		// Now Text fields
		c.weightx = 0.8; c.weighty = 0;
		c.gridx = ++x;
		y = yBackup;
		c.gridy = ++y; panel.add(txtUser,c);
		c.gridy = ++y; panel.add(txtPassword,c);
		c.gridy = ++y;	panel.add(txtPopServer, c); 
		c.gridy = ++y;	panel.add(txtSmtServer, c);

		// Settings Label and Tables.
		JLabel titlePop = new JLabel("Settings to receive mails");
		JLabel titleSmt = new JLabel("Settings to send mails");
		JButton buttonExpandPop = new JButton("Add additional row to receive settings.");
		JButton buttonExpandSmt = new JButton("Add additional row to send settings.");
		MailSettingsTableModel modelPop = new MailSettingsTableModel( settingsContainer.getReceiveProperties() );
		MailSettingsTableModel modelSmt = new MailSettingsTableModel( settingsContainer.getSendProperties() );
		JTable tablePop = new JTable(modelPop);
		JTable tableSmt = new JTable(modelSmt);
		JScrollPane scrollPop = new JScrollPane( tablePop );
		JScrollPane scrollSmt = new JScrollPane( tableSmt );
		buttonExpandPop.addActionListener(addEmptyRowToTableActionListener(modelPop));
		buttonExpandSmt.addActionListener(addEmptyRowToTableActionListener(modelSmt));
		scrollPop.setPreferredSize(new Dimension(TOTAL_FRAME_WIDTH,100));
		scrollSmt.setPreferredSize(new Dimension(TOTAL_FRAME_WIDTH,100));

		c.gridx = 0; x = 0;
		//c.weighty = 0.5;
		//c.weightx = 0.5;
		
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridy = ++y;	panel.add(new JLabel(" "), c);
		c.gridy = ++y;	panel.add(titlePop, c);
		c.gridy = ++y;	panel.add(scrollPop, c);
		c.gridy = ++y;	panel.add(buttonExpandPop, c);
		// ---
		c.gridy = ++y;	panel.add(new JLabel(" "), c);
		c.gridy = ++y;	panel.add(titleSmt, c);
		c.gridy = ++y;	panel.add(scrollSmt, c);
		c.gridy = ++y;	panel.add(buttonExpandSmt, c);
		
		// Submit button
		c.gridy = ++y;	panel.add(new JLabel(" "), c);
		JButton button = new JButton("Save settings.");
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				thisWindow.setVisible(false);
				// Focus on some cells because otherwise changes might not take effect.
				if (tablePop.isEditing())  tablePop.getCellEditor().stopCellEditing();
				if (tableSmt.isEditing())  tableSmt.getCellEditor().stopCellEditing();
				
				Thread thr = new Thread( () ->
				mainWindow.updateMailSettings(
						new MailSettings(
								txtUser.getText(),
								txtPassword.getText(),
								txtPopServer.getText(),
								txtSmtServer.getText(),
								modelPop.getNonEmptyContent(),
								modelSmt.getNonEmptyContent()
								)
						));
				thr.start();
				
				// wait until thread is finished before disposing the window:
				try{
					thr.join();
					dispose();
				} catch (Exception e){ ExceptionVisualizer.show(e); }
				
			}
		});
		c.gridy = ++y; panel.add(button, c);
	
		
		getContentPane().add(panel);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	
	private ActionListener addEmptyRowToTableActionListener(MailSettingsTableModel tabModel){
		return new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				tabModel.addEmtpyRow();
			}
		};
	}
	
	
}
