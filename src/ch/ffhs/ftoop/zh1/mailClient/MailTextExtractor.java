package ch.ffhs.ftoop.zh1.mailClient;

import java.util.Date;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
* MailTextExtractor extracts the content of a email and returns string representations of different mail fields such as subject or date.
* The method writeOldMailContents() is used when a mail is forwarded or replied to.
* 
* @author Daniel Hoop & Mehrdad Zendehzaban
* @version 02.06.2017
*/
public class MailTextExtractor {
	
	private String sentDate, from, recipients, subject, content;
	
	public MailTextExtractor(Message msg){
		try{
			Date sentDate0 = msg.getSentDate();
			if( sentDate0 == null ){
				sentDate = "";
			} else {
				sentDate = sentDate0.toString();
			}
			from = InternetAddress.toString( msg.getFrom() ) ;
			if( from == null ){
				from = "";
			}
			recipients = InternetAddress.toString( msg.getRecipients(MimeMessage.RecipientType.TO) ) ;
			if( recipients == null ){
				recipients = "";
			}
			subject = msg.getSubject() ;
			if( subject == null ){
				subject = "";
			}
			Object content0 = msg.getContent() ;
			if( content0 == null ){
				content = "";
			} else {
				content = content0.toString();
			}
			
		} catch (Exception e) { ExceptionVisualizer.showAndAddMessage(e, "MailTextExtractor(...);\n"); }		
	}
	
	public String getSentDate(){
		return sentDate;
	}
	public String getFrom(){
		return from;
	}
	public String getRecipients(){
		return recipients;
	}
	public String getSubject(){
		return subject;
	}
	public String getContent(){
		return content;
	}
	
	public String writeOldMailContents(){
		return "\n\n\n------------- Original message -------------\n" +
				"Date:      " + getSentDate() + "\n" +
				"Sender:    " + getFrom() + "\n" +
				"Recipient: " + getRecipients() + "\n" +
				"Subject:   " + getSubject() + "\n\n" +
				getContent();
	}
}
