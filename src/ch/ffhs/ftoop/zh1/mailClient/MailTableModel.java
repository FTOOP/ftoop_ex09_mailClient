package ch.ffhs.ftoop.zh1.mailClient;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

/**
* MailTableModel is used in the JTable in MainWindow.
* Vorlage aus Inden S.725
* 
* @author Daniel Hoop & Mehrdad Zendehzaban
* @version 02.06.2017
*/
public class MailTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;

	// Column names
	private static final String[] COLUMN_NAMES = {"Sender", "Subject / Text", "Date" };
	private static final Class<?>[] classes = {String.class, String[].class, String.class};

	// Indices of columns
	private static final int COLUMN_IDX_SENDER = 0;
	private static final int COLUMN_IDX_TITLE = 1;
	private static final int COLUMN_IDX_DATE = 2;
	
	// Data
	private List<Message> mailList;

	// Constructor
	public MailTableModel(List<Message> mailList){
		this.mailList = mailList;
	}
	// Method to update mailList
	public void updateMailList(List<Message> mailList){
		this.mailList = mailList;
		fireTableDataChanged(); // Inden S. 741
	}
	// Method to hand out ArrayList
	public Message getMessageAt(int rowIndex){
		return mailList.get(rowIndex);
	}
	public Message[] getMessageAt(int[] rowIndex) { // List<Message>
		List<Message> mailListSubset = new ArrayList<Message>();
		for(int i : rowIndex){
			mailListSubset.add( mailList.get(i) );
		}
		return mailListSubset.toArray(new Message[0]);
	}
	
	// Add sorter to table. This is here for better encapsulation instead of MainWindow()
	// Here it is also clear that COLUMN_IDX_DATE is the right column number!
	// ***Attention***! JTable and underlying data structure won't be in same order anymore!
	//                  if jTable.getSelectedRow() is used, it has to be converted by using jTable.convertRowIndexToModel()
	public void addSorter(JTable mailTable) {
		TableRowSorter<MailTableModel> sorter = new TableRowSorter<MailTableModel>(this);
		sorter.setComparator(COLUMN_IDX_DATE, new Comparator<Date>(){
			@Override
			public int compare(Date arg0, Date arg1) {
				if(arg0.after(arg1))
					return 1;
				if(arg0.before(arg1))
					return -1;
				return 0;
			}
		});
		List <RowSorter.SortKey> sortKeys  = new ArrayList<RowSorter.SortKey>();
		sortKeys.add(new RowSorter.SortKey(COLUMN_IDX_DATE, SortOrder.DESCENDING));
		sorter.setSortKeys(sortKeys); 
		mailTable.setRowSorter(sorter);
	}
	
	@Override
	public boolean isCellEditable(int row, int column){
		return false;
	}
	@Override
	public String getColumnName(int column){
		return COLUMN_NAMES[column];
	}
	@Override
	public int getColumnCount(){
		return COLUMN_NAMES.length;
	}
	@Override
	public Class<?> getColumnClass(int columnIndex){
		return classes[columnIndex];
	}
	@Override
	public int getRowCount(){
		return mailList.size();
	}
	@Override
	public Object getValueAt(int rowIndex, int columnIndex){
		Message mail = mailList.get(rowIndex);
		try{
			if(columnIndex==COLUMN_IDX_SENDER){
				return InternetAddress.toString( mail.getFrom() );
			}
			if(columnIndex==COLUMN_IDX_TITLE) {
				try {
					//return mail.getSubject().toString();
					//return new String[]{mail.getSubject().toString(), mail.getContent().toString()};
					//System.out.println(mail.getSubject().toString()); System.out.println(mail.getContent().toString());
					return mail.getSubject().toString() + "    " + mail.getContent().toString().substring(0,  Math.min(mail.getContent().toString().length(), 50) );
				} catch (java.io.IOException e) { ExceptionVisualizer.showAndAddMessage(e, "MailTableModel.getAvlueAt(...);\n"); }
			}
				
			if(columnIndex==COLUMN_IDX_DATE) {
				return mail.getSentDate();
			}
				
		} catch (MessagingException e) { ExceptionVisualizer.showAndAddMessage(e, "MailTableModel.getAvlueAt(...);\n"); }

		throw new IllegalArgumentException("Invalid columnIndex " + columnIndex);
	}
	
	
	
}
