package ch.ffhs.ftoop.zh1.mailClient;

/**
* MailAction defines the actions that are allowd when a mail is opened.
*
* @author Daniel Hoop & Mehrdad Zendehzaban
* @version 02.06.2017
*/
public enum MailAction {
	READ, REPLY, FORWARD;
}
