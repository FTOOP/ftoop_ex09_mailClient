package ch.ffhs.ftoop.zh1.mailClient;

import java.util.HashMap;

@Deprecated
public class ZDELETE_GooglemailUtilsWithHashMap extends ZDELETE_MailUtilsWithHashMap {
	
	// Constructor
	public ZDELETE_GooglemailUtilsWithHashMap(String user, String password){
		super(user, password);
	}
	
	public void prepareSessionToReceive(){
		HashMap<String,String> propertyStringsReceive = new HashMap<String,String>();
		propertyStringsReceive.put("mail.store.protocol", "pop3s");
		super.prepareSessionToReceive("pop.gmail.com", propertyStringsReceive);		
	}
	
	public void prepareSessionToSend(){
		HashMap<String,String> propertyStringsSend = new HashMap<String,String>();
		propertyStringsSend.put( "mail.smtp.auth",            "true"           );
		propertyStringsSend.put( "mail.smtp.starttls.enable", "true"           );
		propertyStringsSend.put( "mail.smtp.host",            "smtp.gmail.com" );
		propertyStringsSend.put( "mail.smtp.port",            "587"            );
		super.prepareSessionToSend("smtp.gmail.com", propertyStringsSend); 
	}
}
