package ch.ffhs.ftoop.zh1.mailClient;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

/**
 * FileMover provides the functionality to move mails from one folder to another.
 * @author Daniel Hoop
 *
 */
public class FileMoverWindow extends JFrame {
	private MainWindow mainWindow;
	private String[] files;
	private String fromFolder, toFolder;
	
	public FileMoverWindow(String[] files, String fromFolder, MainWindow mainWindow){
		super("Mail Client");
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		
		this.files = files;
		this.fromFolder = fromFolder.replaceAll("\\\\", "/");
		this.toFolder = null;
		this.mainWindow = mainWindow;
		
		SwingUtilities.invokeLater( () -> initComponents() );
	}
	
	private void initComponents() {
		JPanel contentPane = new JPanel( new GridBagLayout() );
		this.add(contentPane);
		
		
		// Erzeugen der Baumstruktur
		FileTree tree = new FileTree( Path.mails() );
		// Hinzufuegen von Selectionlistener
		tree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
			@Override public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
					TreePath treePath = evt.getPath();
					DefaultMutableTreeNode pathComponent = (DefaultMutableTreeNode) treePath.getLastPathComponent();
					if( pathComponent != null ) {
						toFolder = pathComponent.getUserObject().toString().replaceAll("\\\\", "/");
					}						
			}
		});
		tree.addMouseListener(new MouseAdapter(){
			 @Override public void mousePressed(MouseEvent evt) {
				 if(evt.getClickCount()==2){
						moveFilesAndCloseWindow();
				 }
			 }
		});
		// In JScrollPane einfuegen.
		JScrollPane treePane = new JScrollPane(tree);
		treePane.setPreferredSize(new Dimension(300,100));
		
		// Buttons erstellen.
		JButton cancelButton = new JButton("Cancel");
		JButton okButton = new JButton("OK");
		
		cancelButton.addActionListener(new ActionListener(){
			@Override public void actionPerformed(ActionEvent evt){
				dispose();
			}
		});
		
		okButton.addActionListener(new ActionListener(){
			@Override public void actionPerformed(ActionEvent evt){
				moveFilesAndCloseWindow();
			}
		});
		
		
		
		
		// GridBagStandards
		int x,y;
		x = 0;
		y = -1;
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.WEST;
		c.weightx = 1;
		
		// Add elements to JPanel with right constraints...
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = x; c.gridy = ++y; c.weighty = 1; contentPane.add(treePane, c);
		c.gridwidth = 1;
		c.gridx = x; c.gridy = ++y; c.weighty = 0; contentPane.add(cancelButton, c);
		c.gridx = ++x;                             contentPane.add(okButton, c);
		
		
		// Pack and set visible.
		this.setLocationRelativeTo(null);
		pack();
		setVisible(true);
	}
	
	
	private void moveFilesAndCloseWindow(){
		// If folder was chosen, then move files in background.
		if( toFolder != null && !toFolder.equals(fromFolder) ) {
			setVisible(false);
			Thread thr = new Thread( () -> {
				moveFiles();
				SwingUtilities.invokeLater( () ->  mainWindow.updateTableFromFolderIfNecessary( fromFolder )   );
			});
			thr.start();

			// Dispose of window if thread has finished.
			try{ thr.join();
			} catch (Exception e) { ExceptionVisualizer.show(e); }
			dispose();
		}
	}
	
	
	private void moveFiles(){ //String[] files, String fromFolder, String toFolder) {
		for(String file : files){
			boolean fileMoved = new File(fromFolder + "/" + file).renameTo(new File(toFolder + "/" + file));
			if(!fileMoved) ExceptionVisualizer.show(new Exception(file + "\nwas not moved!"));
		}
	}
	
}
