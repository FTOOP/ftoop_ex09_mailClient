package ch.ffhs.ftoop.zh1.mailClient;

@Deprecated
public class ZDELETE_MailPseudoClass {
	private String sender, recipients, title, text;
	private boolean hasAttachments;
	
	public ZDELETE_MailPseudoClass(String sender, String recipients, String title, String text, boolean hasAttachments){
		this.sender = sender;
		this.recipients = recipients;
		this.title = title;
		this.text = text;
		this.hasAttachments = hasAttachments;
	}
	public String getSender(){
		return sender;
	}
	public String getRecipients(){
		return recipients;
	}
	public String getTitle(){
		return title;
	}
	public String getText(){
		return text;
	}
	public boolean hasAttachments(){
		return hasAttachments;
	}

}

// *** mailList ***
// Create some mailList to fill the table in the GUI.
//ArrayList<Message> mailList = new ArrayList<>(Arrays.asList(new Message[]{
//		new Message("Sender1","Recipients1","Title1","Text1",false)
//		,new Message("Sender2","Recipients2","Title2","Text2",false)
//		}));
