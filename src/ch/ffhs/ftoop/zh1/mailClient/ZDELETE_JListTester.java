package ch.ffhs.ftoop.zh1.mailClient;

import javax.swing.JFrame;
import javax.swing.JList;

@Deprecated
public class ZDELETE_JListTester extends JFrame {
	private static final long serialVersionUID = 1L;


	public ZDELETE_JListTester() {
		super("Mail Client");
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		this.add(new JList<String>(new String[]{"Zeile 1", "Zeile 2"}) );
		this.setVisible(true);
	}
	
	
	public static void main(String[] args) {
		new ZDELETE_JListTester();
	}

}
