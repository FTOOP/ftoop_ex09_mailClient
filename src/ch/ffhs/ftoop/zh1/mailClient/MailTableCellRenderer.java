package ch.ffhs.ftoop.zh1.mailClient;

import java.awt.Component;
import java.text.SimpleDateFormat;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * The MailTableCellRenderer was introduced to show the title of a mail and the first words of the mail content in one cell of a JTable.
 * However, this did not work and therefore this class is essentially useless.
 * 
 * @author Daniel Hoop & Mehrdad Zendehzaban
 * @version 02.06.2017
 */
public class MailTableCellRenderer extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 1L;

	@Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		
        // Renderer for Date
		if(value instanceof java.util.Date){
			this.setText( new SimpleDateFormat("yyyy.MM.dd,   HH:mm:ss,   EEEE").format(value) );			
		}
		
		//cell background color when selected
        if (isSelected) {
            setBackground( table.getSelectionBackground() ); //setBackground(javax.swing.UIManager.getColor("Table.selectionBackground"));
            setForeground( table.getSelectionForeground() ); //setForeground( javax.swing.UIManager.getColor("Table.selectionForeground") );
        } else {
        	setBackground( table.getBackground() );
            setForeground( table.getForeground() );
            
        }
        
        return this;
	}
	
}
